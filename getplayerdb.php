<?php session_start();

if ($_SESSION['Auth'] != 1) {
  header('Location: http://pesolig.com/giris.php');
} ?>
<!DOCTYPE html>

<html>

<header><meta http-equiv="Content-Type" content="text/html; charset=big5">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

</header>
<body>

  <div class="col-lg-5 col-lg-offset-3" style="padding-top:20px;">
    <div class="panel" style="background-color: #DAE0DC; padding:20px; margin-left:130px;">
      <!-- Progress Bar -->
      <div class="progress">
        <div style="color: black; text-align: center;" class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
      </div>
      <!-- //.Progress Bar -->
      <button type="button" class="btn btn-primary btn-sm" style="width: 100%">Oyuncuları Güncelle</button>
      <div class="row" id="errors"></div>
    </div>
  </div>
  
  <!-- Ajax -->

  <script>
    $("button").on("click",function(){
      startProgress();
      alertFunc();
    });
    var myVar;
    var counter = 1;
    var maxCount = 499;
    function startProgress() {
      myVar = setInterval(alertFunc, 35000);
      $(".progress-bar").attr("aria-valuemax",maxCount);
      $(".progress-bar").text(counter + "/" + maxCount);
    }

    function alertFunc() {
      console.log(counter);
      if(counter == maxCount){
        console.log("if");
        clearInterval(myVar);
      }else{
        console.log("else");
        $.get("getPlayer.php",{page:counter},function(data){
          console.log(data);
          obj = JSON.parse(data);
          if(obj.status == 0){
            $(".progress-bar").attr("aria-valuenow",counter);
            $(".progress-bar").css("width",(counter*100)/maxCount + "%");
            $(".progress-bar").text(counter + "/" + maxCount);
          }else{
            $("#errors").append('<p>Page ' + counter +'</p>');
          }
          counter++;
        });
      }
    }
  </script>
  <!-- //.Ajax -->
  
</body>


</body>
</html>
